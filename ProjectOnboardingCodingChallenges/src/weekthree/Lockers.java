package weekthree;

public class Lockers {

	public static void main(String[] args) {

		int[] numberOfLockers = new int[100];
		int currentLoop = 1;
		int storedNumber = 0;

		while (100 > currentLoop) {
			for (int i = 0; i < numberOfLockers.length; i++) {
				if (i == 99 && currentLoop != 101) {
					currentLoop++;
					i = 0;
				}
				storedNumber++;
				if (storedNumber == currentLoop && currentLoop != 101) {
					if (numberOfLockers[i] == 0) {
						numberOfLockers[i] = 1;
					} else {
						numberOfLockers[i] = 0;
					}
					storedNumber = 0;
				}

			}

		}
		int finalNumber = 0;
		for (int i : numberOfLockers) {
			if (i == 1)
				finalNumber++;
		}

		System.out.println("How many lockers open after 100 loops " + finalNumber);
	}

}
